@setlocal ENABLEDELAYEDEXPANSION
@set DSTDIR=classes
@set BINDIR=bin
@set FLAGS=^-uvfe
@set FNAME=%BINDIR%\TomarkyAI-5.jar
@if not exist %DSTDIR% goto NotExistDstdir
@if not exist %BINDIR% mkdir %BINDIR%
@if not exist %FNAME% set FLAGS=^-cvfe
jar %FLAGS% %FNAME% Main -C %DSTDIR%\ .
@goto EndOfBatch
:NotExistDstdir
@echo not exist classes
:EndOfBatch
@endlocal