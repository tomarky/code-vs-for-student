@setlocal
@set SRCDIR=src
@set DSTDIR=classes
@set SRCFILES=%SRCDIR%\*.java
@if not exist %DSTDIR% mkdir %DSTDIR%
javac %* -d %DSTDIR% -sourcepath %SRCDIR% %SRCFILES%
@endlocal
