import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class Main
{
    public static void main(String[] args) throws Exception
    {
        AI ai = new TomarkyAI();
        Pipe pipe = new ByStandardIO();
        GameManager mngr = new GameManager(ai, pipe);
        mngr.run();
    }
}


class GameManager
{
    final AI ai;
    final Pipe pipe;
    GameManager(AI ai, Pipe pipe)
    {
        this.ai = ai;
        this.pipe = pipe;
    }
    void run()
    {
        pipe.sendAIName(ai.getName());
        GameInfomation infomation = pipe.receiveGameInfomtion();
        ai.setGameInfomation(infomation);
        for (int i = 0; i < infomation.pack_total_count; i++)
        {
            TurnState state = pipe.receiveTurnState();
            pipe.sendAICommand(ai.getCommand(state));
        }
    }
}

interface AI
{
    String getName();
    void setGameInfomation(GameInfomation infomation);
    AICommand getCommand(TurnState state);
}

interface Pipe
{
    void sendAIName(String ai_name);
    void sendAICommand(AICommand ai_command);
    GameInfomation receiveGameInfomtion();
    TurnState receiveTurnState();
}

interface Pack
{
    int get(int x, int y);
    Pack rotate(int rotation);
    Pack appendObstacles(long count);
    int getObstacles();
}

interface Field
{
    int get(int x, int y);
    long getObstacleCount();
    List<Field> getAllDropPattern(Pack pack);
    long getScore();
    int getChain();
    int getLastDropPosition();
    int getLastDropRotaion();
    int getMaxHeight();
    int getBlockCount();
    int getFieldObstacleCount();
}

class AICommand
{
    final int position;
    final int rotation;
    AICommand(int p, int r)
    {
        position = p;
        rotation = r;
    }
    @Override
    public String toString()
    {
        return String.format("%d %d", position, rotation);
    }
}

class GameInfomation
{
    final int field_width;
    final int field_height;
    final int field_max_height;
    final int pack_edge_length;
    final long pack_max_capacity;
    final int extinct_sum;
    final int pack_total_count;
    final List<Pack> packs;
    
    GameInfomation(int w, int h, int t, int s, int n, List<int[]> ps)
    {
        field_width = w;
        field_height = h;
        field_max_height = h + t;
        pack_edge_length = t;
        pack_max_capacity = (long)(t * t);
        extinct_sum = s;
        pack_total_count = n;
        
        Pack[] temp = new Pack[n];
        for (int i = 0; i < n; i++)
        {
            temp[i] = new PackData(ps.get(i), 0);
        }
        
        packs = Collections.unmodifiableList(Arrays.asList(temp));
    }
    
    class PackData implements Pack
    {
        final int[] blocks;
        final int rotation;
        final int obstacles;
        final String id;
        PackData(int[] b, int r, int o)
        {
            blocks = b;
            rotation = r;
            obstacles = o;
            id = identity();
        }
        PackData(int[] b, int r)
        {
            this(b, r, 0);
        }
        @Override
        public int hashCode()
        {
            return id.hashCode();
        }
        @Override
        public boolean equals(Object o)
        {
            if (o == this) return true;
            if (o == null) return false;
            if (!getClass().equals(o.getClass())) return false;
            return id.equals(((PackData)o).id);
        }
        String identity()
        {
            StringBuilder sb = new StringBuilder();
            String[] temp = new String[pack_edge_length];
            for (int x = 0; x < pack_edge_length; x++)
            {
                for (int y = pack_edge_length - 1; 0 <= y; y--)
                {
                    int b = get(x, y);
                    if (b == 0)
                    {
                        continue;
                    }
                    sb.append(b);
                    sb.append('-');
                }
                temp[x] = sb.toString();
                sb.delete(0, sb.length());
            }
            int i = 0;
            while (i < pack_edge_length && temp[i].length() == 0)
            {
                i++;
            }
            int k = pack_edge_length - 1;
            while (0 <= k && temp[k].length() == 0)
            {
                k--;
            }
            for ( ; i <= k; i++)
            {
                sb.append(temp[i]);
                sb.append(':');
            }
            return sb.toString();
        }
        public int get(int x, int y)
        {
            int tx = x, ty = y;
            switch (rotation)
            {
            case 1:
                tx = y;
                ty = (pack_edge_length - 1) - x;
                break;
            case 2:
                tx = (pack_edge_length - 1) - x;
                ty = (pack_edge_length - 1) - y;
                break;
            case 3:
                tx = (pack_edge_length - 1) - y;
                ty = x;
                break;
            }
            return blocks[ty * pack_edge_length + tx];
        }
        public Pack rotate(int r)
        {
            return new PackData(blocks, r, obstacles);
        }
        public Pack appendObstacles(long count)
        {
            int[] temp = Arrays.copyOf(blocks, blocks.length);
            int obs = 0;
            for (int i = 0; count > 0L && i < temp.length; i++)
            {
                if (temp[i] == 0)
                {
                    temp[i] = extinct_sum + 1;
                    count--;
                    obs++;
                }
            }
            return new PackData(temp, rotation, obs);
        }
        public int getObstacles()
        {
            return obstacles;
        }
    }
        
    FieldData makeNewFieldData(long ob)
    {
        return new FieldData(ob, new int[field_max_height * field_width]);
    }
    
    class FieldData implements Field
    {
        final long obstacles;
        final int[] blocks;
        final long score;
        final int chain;
        final int position;
        final int rotation;
        final int max_height;
        final int block_count;
        final int field_obstacle_count;
        
        FieldData(long ob, int[] bl, int ch, long sc, int pos, int rot)
        {
            obstacles = ob;
            blocks = bl;
            chain = ch;
            score = sc;
            position = pos;
            rotation = rot;
            int i = 0;
            while (i < bl.length && bl[i] == 0)
            {
                i++;
            }
            max_height = field_max_height - (i / field_width) - 1;
            int bc = 0;
            int foc = 0;
            for ( ; i < bl.length; i++)
            {
                if (bl[i] > 0)
                {
                    bc++;
                    if (bl[i] > extinct_sum)
                    {
                        foc++;
                    }
                }
            }
            block_count = bc;
            field_obstacle_count = foc;
        }
        FieldData(long ob, int[] bl)
        {
            this(ob, bl, 0, 0L, Integer.MAX_VALUE, -1);
        }
        
        public int get(int x, int y)
        {
            return blocks[y * field_width + x];
        }
        
        public long getObstacleCount()
        {
            return obstacles;
        }
        
        public int getChain()
        {
            return chain;
        }
        
        public long getScore()
        {
            return score;
        }
        
        public int getLastDropPosition()
        {
            return position;
        }
        
        public int getLastDropRotaion()
        {
            return rotation;
        }
        
        public int getMaxHeight()
        {
            return max_height;
        }
        
        public int getBlockCount()
        {
            return block_count;
        }
        
        public int getFieldObstacleCount()
        {
            return field_obstacle_count;
        }
        
        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.append(obstacles);
            sb.append(", ");
            sb.append(score);
            sb.append(", ");
            sb.append(chain);
            sb.append(System.lineSeparator());
            for (int x = 0; x < field_width; x++)
            {
                for (int i = (field_max_height - 1) * field_width + x; 0 <= i; i -= field_width)
                {
                    sb.append(String.format("%2d", blocks[i]));
                }
                sb.append(System.lineSeparator());
            }
            return sb.toString();
        }
        
        boolean checkCorrectPos(Pack pack, int pos)
        {
            if (pos < 0)
            {
                for (int y = 0; y < pack_edge_length; y++)
                {
                    for (int x = 0; x < -pos; x++)
                    {
                        if (pack.get(x, y) != 0)
                        {
                            return false;
                        }
                    }
                }
            }
            else if (field_width < pos + pack_edge_length)
            {
                for (int y = 0; y < pack_edge_length; y++)
                {
                    for (int x = pos + pack_edge_length - field_width; 0 < x; x--)
                    {
                        if (pack.get(pack_edge_length - x, y) != 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        
        boolean drop(int[] temp)
        {
            boolean droped = false;
            for (int x = 0; x < field_width; x++)
            {
                int j = (field_max_height - 1) * field_width + x;
                for (int i = j; field_width <= i; i -= field_width)
                {
                    if (temp[i] != 0)
                    {
                        continue;
                    }
                    if (j > i)
                    {
                        j = i - field_width;
                    }
                    while (0 <= j && temp[j] == 0)
                    {
                        j -= field_width;
                    }
                    if (j < 0)
                    {
                        break;
                    }
                    temp[i] = temp[j];
                    temp[j] = 0;
                    droped = true;
                }
            }
            return droped;
        }
        
        int eraseHorizontal(int[] temp, boolean[] flags)
        {
            int count = 0;
            for (int y = 0; y < field_max_height; y++)
            {
                int d = y * field_width;
                for (int i = 0; i < field_width; i++)
                {
                    int j = i + d;
                    if (temp[j] == 0)
                    {
                        continue;
                    }
                    int sum = temp[j];
                    int k = i + 1;
                    for ( ; sum < extinct_sum && k < field_width; k++)
                    {
                        int bl = temp[k + d];
                        if (bl == 0)
                        {
                            break;
                        }
                        sum += bl;
                    }
                    if (sum == extinct_sum)
                    {
                        k += d;
                        count += k - j;
                        for ( ; j < k; j++)
                        {
                            flags[j] = true;
                        }
                    }
                }
            }
            return count;
        }
        
        int eraseVertical(int[] temp, boolean[] flags)
        {
            int count = 0;
            for (int x = 0; x < field_width; x++)
            {
                for (int i = (field_max_height - 1) * field_width + x; 0 <= i; i -= field_width)
                {
                    if (temp[i] == 0)
                    {
                        continue;
                    }
                    int sum = temp[i];
                    int j = i - field_width;
                    for ( ; sum < extinct_sum && 0 <= j; j -= field_width)
                    {
                        int bl = temp[j];
                        if (bl == 0)
                        {
                            break;
                        }
                        sum += bl;
                    }
                    if (sum == extinct_sum)
                    {
                        for (int k = i; j < k; k -= field_width)
                        {
                            flags[k] = true;
                            count++;
                        }
                    }
                }
            }
            return count;
        }
        
        int eraseSlash(int[] temp, boolean[] flags)
        {
            int count = 0;
            for (int y = field_max_height - 1; -field_width < y; y--)
            {
                int i = y * field_width;
                for (int x = 0; x < field_width; x++)
                {
                    if (temp.length <= i)
                    {
                        break;
                    }
                    if (0 <= i && 0 < temp[i])
                    {
                        int sum = temp[i];
                        int z = x + 1;
                        int j = i + field_width + 1;
                        for ( ; sum < extinct_sum && z < field_width; z++)
                        {
                            if (temp.length <= j || temp[j] == 0)
                            {
                                break;
                            }
                            sum += temp[j];
                            j += field_width + 1;
                        }
                        if (sum == extinct_sum)
                        {
                            count += z - x;
                            for (int k = i; k < j; k += field_width + 1)
                            {
                                flags[k] = true;
                            }
                        }
                    }
                    i += field_width + 1;
                }
            }                
            return count;
        }
        
        int eraseBackSlash(int[] temp, boolean[] flags)
        {
            int count = 0;
            for (int y = field_max_height + field_width - 1; 0 <= y; y--)
            {
                int i = y * field_width;
                for (int x = 0; x < field_width; x++)
                {
                    if (i < 0)
                    {
                        break;
                    }
                    if (i < temp.length && 0 < temp[i])
                    {
                        int sum = temp[i];
                        int z = x + 1;
                        int j = i - (field_width - 1);
                        for ( ; sum < extinct_sum && z < field_width; z++)
                        {
                            if (j < 0 || temp[j] == 0)
                            {
                                break;
                            }
                            sum += temp[j];
                            j -= field_width - 1;
                        }
                        if (sum == extinct_sum)
                        {
                            count += z - x;
                            for (int k = i; j < k; k -= field_width - 1)
                            {
                                flags[k] = true;
                            }
                        }
                    }
                    i -= field_width - 1;
                }
            }
            return count;
        }
        
        
        int erase(int[] temp)
        {
            boolean[] flags = new boolean[temp.length];
            int count = 0;
            count += eraseHorizontal(temp, flags);
            count += eraseVertical(temp, flags);
            count += eraseSlash(temp, flags);
            count += eraseBackSlash(temp, flags);
            for (int i = 0; i < temp.length; i++)
            {
                if (flags[i])
                {
                    temp[i] = 0;
                }
            }
            return count;
        }
        
        FieldData doDrop(int[] temp, long obs, int pos, int rot)
        {
            int ch = 0;
            long sc = 0L;
            while (drop(temp))
            {
                int count = erase(temp);
                if (count == 0)
                {
                    break;
                }
                ch++;
                sc += (long)(count / 2) * (long)Math.floor(Math.pow(1.3, ch));
            }
            obs -= sc / 5L;
            return new FieldData(obs, temp, ch, sc, pos, rot);
        }
        
        boolean isAlive()
        {
            return max_height < field_height;
        }
        
        public List<Field> getAllDropPattern(Pack pack)
        {
            long obs = obstacles;
            if (obstacles > 0L)
            {
                pack = pack.appendObstacles(Math.min(obstacles, pack_max_capacity)); 
                obs -= (long)pack.getObstacles();
            }
            List<Field> list = new ArrayList<>();
            Set<Pack> unique = new HashSet<>();
            for (int r = 0; r < 4; r++)
            {
                if (unique.add(pack) == false)
                {
                    continue;
                }
                for (int i = 1 - pack_edge_length; i <= field_width; i++)
                {
                    if (checkCorrectPos(pack, i) == false)
                    {
                        continue;
                    }
                    int[] temp = Arrays.copyOf(blocks, blocks.length);
                    for (int x = 0; x < pack_edge_length; x++)
                    {
                        int tx = x + i;
                        if (tx < 0 || field_width <= tx)
                        {
                            continue;
                        }
                        for (int y = 0; y < pack_edge_length; y++)
                        {
                            temp[y * field_width + tx] = pack.get(x, y);
                        }
                    }
                    FieldData field = doDrop(temp, obs, i, r);
                    if (field.isAlive())
                    {
                        list.add(field);
                    }
                }
                pack = pack.rotate(r + 1);
            }
            return list;
        }
    }
}

class TurnState
{
    final int turn_count;
    final long game_time;
    final Field my_field;
    final Field rival_field;
    
    TurnState(int c, long t, Field my, Field rival)
    {
        turn_count = c;
        game_time = t;
        my_field = my;
        rival_field = rival;
    }   
}


class ByStream implements Pipe
{
    private final Scanner in;
    private final PrintStream out;
    private GameInfomation infomation = null;
    ByStream(InputStream i, PrintStream o)
    {
        in = new Scanner(i);
        out = o;
    }
    public void sendAIName(String ai_name)
    {
        out.println(ai_name);
        out.flush();
    }
    public void sendAICommand(AICommand ai_command)
    {
        out.println(ai_command);
        out.flush();
    }
    public GameInfomation receiveGameInfomtion()
    {
        int w = in.nextInt();
        int h = in.nextInt();
        int t = in.nextInt();
        int s = in.nextInt();
        int n = in.nextInt();
        List<int[]> ps = new ArrayList<>(n);
        int block_size = t * t;
        for (int i = 0; i < n; i++)
        {
            int[] blocks = new int[block_size];
            for (int j = 0; j < block_size; j++)
            {
                blocks[j] = in.nextInt();
            }
            ps.add(blocks);
            in.next("END");
        }
        infomation = new GameInfomation(w, h, t, s, n, ps);
        return infomation;
    }
    
    private Field readField()
    {
        long obstacles = in.nextLong();
        GameInfomation.FieldData field = infomation.makeNewFieldData(obstacles);
        int dx = infomation.pack_edge_length * infomation.field_width;
        for (int y = 0; y < infomation.field_height; y++)
        {
            for (int x = 0; x < infomation.field_width; x++)
            {
                field.blocks[x + dx]  = in.nextInt();
            }
            dx += infomation.field_width;
        }
        in.next("END");
        return (Field)field;
    }
    
    public TurnState receiveTurnState()
    {
        int c = in.nextInt();
        long t = in.nextLong();
        Field my_field = readField();
        Field rival_field = readField();
        return new TurnState(c, t, my_field, rival_field);
    }
}

class ByStandardIO extends ByStream implements Pipe
{
    ByStandardIO()
    {
        super(System.in, System.out);
    }
}

class TomarkyAI implements AI
{
    GameInfomation infomation = null;
    EvaluationField evalF = new EvaluationField();
    EvaluationStep evalS = new EvaluationStep();
    Step operation = null;
    
    public String getName()
    {
        return "TomarkyAI-5";
    }
    
    public void setGameInfomation(GameInfomation infomation)
    {
        this.infomation = infomation;
        
    }
    
    public AICommand getCommand(TurnState state)
    {
        Pack pack = infomation.packs.get(state.turn_count);
        List<Field> my_first_steps = state.my_field.getAllDropPattern(pack);
        Field attack = null;
        for (Field field : state.rival_field.getAllDropPattern(pack))
        {
            if (attack == null)
            {
                attack = field;
                continue;
            }
            if (field.getObstacleCount() < attack.getObstacleCount())
            {
                attack = field;
            }
        }
        Field guard = null;
        for (Field my_field : my_first_steps)
        {
            if (guard == null)
            {
                guard = my_field;
                continue;
            }
            if (my_field.getObstacleCount() < guard.getObstacleCount())
            {
                guard = my_field;
            }
        }
        if (attack != null && attack.getObstacleCount() < -5L)
        {
            if (guard != null && guard.getObstacleCount() < state.my_field.getObstacleCount())
            {
                operation = null;
                return new AICommand(guard.getLastDropPosition(), guard.getLastDropRotaion());
            }
        }     
        if (operation == null || operation.field.getObstacleCount() != state.my_field.getObstacleCount())
        {
            my_first_steps = state.my_field.getAllDropPattern(pack);
            if (my_first_steps.isEmpty())
            {
                operation = null;
                return new AICommand(0, 0);
            }
            
            int pattern_size = (infomation.field_width + infomation.pack_edge_length) * 4;
            
            List<Step> step_list = new ArrayList<>(pattern_size);
            for (Field field : my_first_steps)
            {
                step_list.add(new Step(field));
            }
            
            Queue<Step> betters = new PriorityQueue<>(100, evalS);
            
            Queue<Step> prebetters = new PriorityQueue<>(pattern_size * pattern_size, evalS);
            
            int turn = state.turn_count;
            
            int half = infomation.field_height * infomation.field_width / 2;
            
            int thinking = state.my_field.getBlockCount() > half ? 8 : 50;
            if (operation != null)
            {
                thinking = Math.min(Math.max(3, operation.depth + 1), thinking);
            }
            
            for (int i = 0; i < thinking; i++)
            {
                turn++;
                if (infomation.pack_total_count <= turn )
                {
                    break;
                }
                prebetters.clear();
                pack = infomation.packs.get(turn);
                for (Step step : step_list)
                {
                    List<Field> field_list = step.field.getAllDropPattern(pack);
                    for (Field field : field_list)
                    {
                        prebetters.add(new Step(field, step));
                    }
                }
                Step best = prebetters.poll();
                if (best == null)
                {
                    break;
                }
                betters.add(best);
                step_list.clear();
                int max_pattern = 53;
                if (prebetters.size() <= max_pattern)
                {
                    step_list.addAll(prebetters);
                }
                else
                {
                    int edge = 1;
                    int rems = max_pattern - edge * 2;
                    Step[] steps = prebetters.toArray(new Step[prebetters.size()]);
                    for (int j = 0; j < edge; j++)
                    {
                        step_list.add(steps[j]);
                        step_list.add(steps[steps.length - j - 1]);
                    }
                    int k = steps.length - edge * 2;
                    for (int j = 0; j < rems; j++)
                    {
                        step_list.add(steps[edge + j * k / rems]);
                    }
                }
            }
            Step best = betters.poll();
            if (best == null)
            {
                operation = null;
                if (my_first_steps.isEmpty())
                {
                    return new AICommand(0, 0);
                }
                Field f = my_first_steps.get(0);
                return new AICommand(f.getLastDropPosition(), f.getLastDropRotaion());
            }
            int upup = infomation.field_height * infomation.field_width * 3 / 5;
            if (state.my_field.getBlockCount() >= upup)
            {
                while (!betters.isEmpty())
                {
                    Step temp = betters.poll();
                    int d1 = temp.depth;
                    int d2 = best.depth;
                    if (d1 > d2)
                    {
                        continue;
                    }
                    if (d1 == d2)
                    {
                        if (temp.field.getBlockCount() > best.field.getBlockCount())
                        {
                            continue;
                        }
                        if (temp.field.getScore() < best.field.getScore())
                        {
                            continue;
                        }
                    }
                    best = temp;
                }
            }
            operation = best.reverse();
        }
        Field field = operation.field;
        operation = operation.other;
        return new AICommand(field.getLastDropPosition(), field.getLastDropRotaion());
    }
    
    class Step
    {
        final Field field;
        final Step other;
        final long max_score;
        final long min_obstacles;
        final int max_chain;
        final int depth;
        Step(Field f, Step o)
        {
            field = f;
            other = o;
            if (o == null)
            {
                max_score = field.getScore();
                min_obstacles = field.getObstacleCount();
                max_chain = field.getChain();
                depth = 1;
            }
            else
            {
                max_score = Math.max(o.max_score, field.getScore());
                min_obstacles = Math.min(o.min_obstacles, field.getObstacleCount() - o.field.getObstacleCount());
                max_chain = Math.max(o.max_chain, field.getChain());
                depth = 1 + o.depth;
            }
        }
        Step(Field f)
        {
            this(f, null);
        }
        Step reverse()
        {
            Step rev = null;
            Step temp = this;
            while (temp.other != null)
            {
                rev = new Step(temp.field, rev);
                temp = temp.other;
            }
            return new Step(temp.field, rev);
        }
    }
    
    class EvaluationStep implements Comparator<Step>
    {
        public int compare(Step s1, Step s2)
        {
            Field f1 = s1.field;
            Field f2 = s2.field;
            if (s1.min_obstacles < s2.min_obstacles)
            {
                return -1;
            }
            if (s1.min_obstacles > s2.min_obstacles)
            {
                return 1;
            }
            if (f1.getObstacleCount() < f2.getObstacleCount())
            {
                return -1;
            }
            if (f1.getObstacleCount() > f2.getObstacleCount())
            {
                return 1;
            }
            if (f1.getMaxHeight() != f2.getMaxHeight())
            {
                return f2.getMaxHeight() - f1.getMaxHeight();
            }
            return f2.getBlockCount() - f1.getBlockCount();
        }
    }
    
    class EvaluationField implements Comparator<Field>
    {
        public int compare(Field f1, Field f2)
        {
            if (f1.getMaxHeight() != f2.getMaxHeight())
            {
                return f2.getMaxHeight() - f1.getMaxHeight();
            }
            return f2.getBlockCount() - f1.getBlockCount();
        }
    }
}