2016/10/17～2016/11/15に開催されたCODE VS for STUDENTの予選に使ったAIのコード(※学生以外は予選だけ参加可能)

https://student.codevs.jp/

予選順位Normal 20位

version5
    攻撃判定や探索範囲に関して変更を加えた、version4より弱くなった

version4fix
    version1から使ってるドロップ配置計算処理にあったバグを修正(version1-3まではこの修正がされてないバグ入りのまま)

version4
    方針を大きく変え、数十個分のドロップ配置パターンを探索

version3fix
    version3のAI思考にエラーになるバグがあったので修正

version3
    方針を大きく変え、1つ目のドロップの配置の全パターンからそれぞれ2ドロップ分の配置パターンを探索

version2fix
    version2のAI思考にエラーになるバグがあったので修正
 
version2
    ドロップ2つ分を配置できる全パターンで一番良さそうなのを選ぶだけ

version1
    ドロップ1つ分を配置できる全パターンで一番良さそうなのを選ぶだけ
 

 